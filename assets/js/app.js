
// Menu toggle
// https://inclusive-components.design/menus-menu-buttons/

const menuToggle = (function () {

	const State = {
		menuSelector: '',
		toggleCaption: 'Site settings'
	};


	// toggle the button aria-expanded state
	const toggleCollapsible = (e) => {
		let button = e.target.closest ('button');
		let collapsible = button.nextElementSibling;
		let expanded = button.getAttribute ('aria-expanded') === 'true' || false;
		button.setAttribute ('aria-expanded', !expanded);
		collapsible.hidden = expanded;
	};


	const init = (menuSelector, toggleCaption = State.toggleCaption) => {
		// update State
		State.menuSelector = menuSelector;
		State.menu = document.querySelector (menuSelector);
		if (! State.menu) return console.warn ('menuToggle: menu selector not found');

		// setup toggle button
		State.toggleCaption = toggleCaption;
		let toggleButton = document.querySelector ('[data-toggle-button]');
		toggleButton.addEventListener ('click', toggleCollapsible);
	};


	return {
		"init": init,
	}

})();

menuToggle.init ('#site-settings-navigation', 'Préférences du site');



// Read mode

const toggleReadMode = (() => {

	const toggleMode = (e) => {
		let url = window.location.href;
		window.open ('read-mode.html?url=' + url);
	};

	const toggle = document.querySelector ('[data-toggle-readability]');
	if (null === toggle) return console.warn ('toggleReadMode: toggle not found', '[data-toggle-readability]');
	toggle.addEventListener ('click', toggleMode);

})();


// Easings
//@prepros-prepend ../libs/EasingFunctions.js


// list animation: items delay
const listDelay = (function () {

	const init = (selector = "ul", delay = 200) => {
		let listItems = document.querySelectorAll (`${selector} li`);
		if (!listItems) return false;
		let itemDelay = 0;

		[...listItems].map ((li) => {
			li.style.setProperty ('animation-delay', `${itemDelay}ms`);
			itemDelay += EasingFunctions.easeOutCubic(delay) / 1000;
			return li;
		});
	};

	return {
		"init": init,
	}
})();

listDelay.init ('.menu', 50);



// Dark mode

const ThemeChanger = (function (){

	const State = {
		toggle: null,
		selector: '',
	};

	const toggleTheme = (e) => {
		e.preventDefault();
		let theme = e.target.closest (State.selector).getAttribute ('data-theme-toggle') || false;
		if (!theme) return console.warn ('ThemeChanger: no theme found', State);

		let body = document.querySelector ('body');
		let currentTheme = body.getAttribute ('data-theme') || 'default';

		State.toggle.setAttribute ('data-theme-toggle', currentTheme);
		body.setAttribute ('data-theme', theme);
	};

	const init = (selector = "[data-theme-toggle]") => {
		State.selector = selector;
		State.toggle = document.querySelector (selector);
		if (null === State.toggle) return console.warn ('ThemeChanger: toggle button not found', State);
		State.toggle.addEventListener ('click', toggleTheme);
	};

	return {
		init: init,
	}

})();

ThemeChanger.init ();



// Remove the animation class ".roll-in-blurred-right" when
// to allow transforms on active state.

const resetAnimation = (function () {
	let elements = document.querySelectorAll ('.roll-in-blurred-right');
	[...elements].map (el => {
		el.addEventListener ('animationend', function (e) {
			e.target.classList.remove ('roll-in-blurred-right');
		}, false);
	});
})();




// ---

// inject the toggle button
// <button data-toggle-button aria-expanded="false">Menu</button>
/*
const buildToggleButton = () => {

	let container = document.querySelector (State.menuSelector);
	let navToggle = document.createElement ('button');

	// setup button
	navToggle.setAttribute ('type', 'button');
	navToggle.setAttribute ('data-toggle-button', '');
	navToggle.setAttribute ('aria-expanded', false); // collapsed by default
	navToggle.innerHTML = State.toggleCaption;
	navToggle.addEventListener ('click', toggleCollapsible);

	// insert the button before the first node of the container
	container.insertBefore (navToggle, container.childNodes[0]);
};
*/
