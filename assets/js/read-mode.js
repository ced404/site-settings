// Mercury Web Parser API Wrapper
// https://mercury.postlight.com/web-parser/

// Import config
//@prepros-prepend .config.js

const WebParser = (function () {

	const Config = {
		API_URL: READMODE_API_URL,
		API_KEY: READMOE_API_KEY,
	};

	// get read mode version of url
	const parseURL = function (url) {

		return new Promise (function (resolve, reject) {
			let req = new XMLHttpRequest ();
			req.withCredentials = false;

			req.open ('GET', Config.API_URL + encodeURIComponent(url), true);
			req.setRequestHeader ('Content-type', 'application/json');
			req.setRequestHeader ('x-api-key', Config.API_KEY);

			req.onreadystatechange = function () {
				if (req.readyState == XMLHttpRequest.DONE) {
					if (req.status == 200) {
						resolve (req.responseText);
					}
					else {
						reject (Error (req.statusText));
					}
				}
			};

			req.onerror = function () {
				reject (Error ('Network Error'));
			};

			req.send ();
		});
	};

	return {
		'parseURL': parseURL,
	}
})();




// ReadMode - convert and display the parsed content of a URL

const ReadMode = (function () {

	const State = {
		container: null,
		url: '',
	};


	const parseURL = () => {
		WebParser.parseURL (State.url).then (
			function (results) {
				let data = JSON.parse (results);
				let title = `<h1>${data.title}</h1>`;
				State.container.innerHTML = title + data.content;
			},
			function (results) {
				console.error ('error', results);
			},
		);
	};

	const init = ({container, url}) => {

		let urlParams = new URLSearchParams(window.location.search);
		State.url = url || urlParams.get ('url');
		if (!State.url) return console.warn ('ReadMode: bad url', url);

		State.container = document.querySelector (container);
		if (null === State.container) return console.warn ('ReadMode: container not found', container);
		parseURL ();
		console.log('State', State);
	};

	return {
		init: init,
	}
})();



// Get stripped version of URL and display in the #output element.
// ReadMode.init ({
// 	container: '#output',
// 	url: 'https://www.cregg.org/commissions/prevention-des-cancers-par-endoscopie/fiches-de-recommandations-prevention-des-cancers-par-endoscopie/prevention-du-cancer-de-l-estomac-2/'
// });



// Parse the URL passed in the query string (?url=…) of the current (window.)location
ReadMode.init ({container: '#output'});
